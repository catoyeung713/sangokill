import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.modules';
import { HttpClientModule } from '@angular/common/http';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from 'app/app.component';
import { HeadComponent } from 'layout/head.component';
import { LeftPanelComponent } from 'layout/left-panel.component';
import { DashboardComponent } from 'components/dashboard/dashboard.component';
import { LoginComponent } from 'components/account/login/login.component';
import { LogoutComponent } from 'components/account/logout/logout.component';
import { UsersComponent } from 'components/users/users.component';

import { AppConfig } from 'config/config';
import { Helpers } from 'helpers/helpers';
import { TokenService } from 'services/token.service';

@NgModule({
  declarations: [
    AppComponent,
    HeadComponent,
    LeftPanelComponent,
    DashboardComponent,
    LoginComponent,
    LogoutComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatFormFieldModule,
    MatSidenavModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [AppConfig, Helpers, TokenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
