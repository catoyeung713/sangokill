import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Helpers } from 'helpers/helpers';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {

  constructor(private helpers: Helpers, private router: Router) { }

  ngOnInit() {
  }
} 
