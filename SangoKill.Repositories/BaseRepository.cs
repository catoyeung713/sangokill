﻿using Microsoft.EntityFrameworkCore;
using SangoKill.Interfaces;
using SangoKill.Models;
using SangoKill.Models.Context;
using SangoKill.Models.EntityBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SangoKill.Repositories
{
    public abstract class BaseRepository<M> where M : BaseModel
    {
        protected ApplicationContext DbContext;
        protected readonly DbSet<M> DbSet;
        public BaseRepository(ApplicationContext dbContext)
        {
            DbContext = dbContext;
        }
        public virtual M Add(M entity)
        {
            DbSet.Add(entity);
            return entity;
        }
        public virtual M GetSingle(Expression<Func<M, bool>> where)
        {
            return DbSet.SingleOrDefault(where);
        }
        public virtual List<M> GetMultiple(Expression<Func<M, bool>> where)
        {
            return DbSet.Where<M>(where).ToList();
        }
        public virtual List<M> GetAll()
        {
            return DbSet.ToList();
        }
        public virtual M Update(M entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
            DbContext.SaveChanges();
            return entity;
        }
        public virtual void Delete(int id)
        {
            M entityToDelete = DbSet.Find(id);
            DbSet.Remove(entityToDelete);
        }
        public virtual void Delete(Expression<Func<M, bool>> where)
        {
            IEnumerable<M> objects = DbSet.Where<M>(where).AsEnumerable();
            foreach (M obj in objects)
                DbSet.Remove(obj);
        }
        public virtual IQueryable<M> GetQueryable()
        {
            return DbSet.AsQueryable();
        }
        public virtual IQueryable<M> GetQueryableNoTracking()
        {
            return DbSet.AsQueryable().AsNoTracking();
        }
    }
}