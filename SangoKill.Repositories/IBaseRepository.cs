﻿using SangoKill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SangoKill.Repositories
{
    public interface IBaseRepository<M> where M : IBaseModel
    {
        M Add(M entity);
        M GetSingle(Expression<Func<M, bool>> where);
        M GetMultiple(Expression<Func<M, bool>> where);
        List<M> GetAll();
        M Update(M entity);
        void Delete(int id);
        void Delete(Expression<Func<M, bool>> where);
        IQueryable<M> GetQueryable();
        IQueryable<M> GetQueryableNoTracking();
    }
}
