﻿using Microsoft.EntityFrameworkCore;
using SangoKill.Interfaces;
using SangoKill.Models;
using SangoKill.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SangoKill.Repositories
{
    public class UserRepository : IUserRepository
    {
        protected ApplicationContext DbContext;
        protected readonly DbSet<User> DbSet;
        public UserRepository(ApplicationContext dbContext)
        {
            DbContext = dbContext;
        }

        public User Add(User entity)
        {
            DbSet.Add(entity);
            return entity;
        }

        public virtual User GetSingle(Expression<Func<User, bool>> where)
        {
            return DbSet.SingleOrDefault(where);
        }
        public virtual List<User> GetMultiple(Expression<Func<User, bool>> where)
        {
            return DbSet.Where<User>(where).ToList();
        }
        public virtual List<User> GetAll()
        {
            return DbSet.ToList();
        }
        public virtual User Update(User entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
            DbContext.SaveChanges();
            return entity;
        }
        public virtual void Delete(int id)
        {
            User entityToDelete = DbSet.Find(id);
            DbSet.Remove(entityToDelete);
        }
        public virtual void Delete(Expression<Func<User, bool>> where)
        {
            IEnumerable<User> objects = DbSet.Where<User>(where).AsEnumerable();
            foreach (User obj in objects)
                DbSet.Remove(obj);
        }
        public virtual IQueryable<User> GetQueryable()
        {
            return DbSet.AsQueryable();
        }
        public virtual IQueryable<User> GetQueryableNoTracking()
        {
            return DbSet.AsQueryable().AsNoTracking();
        }
    }
}
