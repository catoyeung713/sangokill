﻿using SangoKill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SangoKill.Repositories
{
    public interface IUserRepository
    {
        User Add(User entity);
        User GetSingle(Expression<Func<User, bool>> where);
        List<User> GetMultiple(Expression<Func<User, bool>> where);
        List<User> GetAll();
        User Update(User entity);
        void Delete(int id);
        void Delete(Expression<Func<User, bool>> where);
        IQueryable<User> GetQueryable();
        IQueryable<User> GetQueryableNoTracking();
    }
}
