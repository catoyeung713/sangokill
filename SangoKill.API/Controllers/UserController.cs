﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SangoKill.Interfaces;
using SangoKill.Maps;
using SangoKill.Services;
using SangoKill.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SangoKill.Web.API.Controllers
{
    [Route("api/users")]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IMapper Mapper;
        private readonly IUserMap UserMap;
        private readonly IUserService UserService;

        public UserController(IUserMap userMap, IUserService userService)
        {
            UserMap = userMap;
            UserService = userService;
        }

        // GET api/users

        [HttpGet]
        public List<UserViewModel> Get()
        {
            return Mapper.Map<List<UserViewModel>>(UserService.GetAll());
        }

        // GET api/users/5

        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/users

        [HttpPost]
        public void Post([FromBody]string user)
        {

        }

        // PUT api/users/5

        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string user)
        {

        }

        // DELETE api/users/5

        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }
    }
}
