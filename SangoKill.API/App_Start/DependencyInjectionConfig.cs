﻿using Microsoft.Extensions.DependencyInjection;
using SangoKill.Interfaces;
using SangoKill.Maps;
using SangoKill.Models.Context;
using SangoKill.Repositories;
using SangoKill.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SangoKill.Web.API.App_Start
{
    public class DependencyInjectionConfig
    {
        public static void AddScope(IServiceCollection services)
        {
            services.AddScoped<IApplicationContext, ApplicationContext>();
            services.AddScoped<IUserMap, UserMap>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
        }
    }
}
