﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SangoKill.ViewModels
{
    public class UserViewModel : IBaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
