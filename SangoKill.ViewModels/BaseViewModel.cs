﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SangoKill.ViewModels
{
    public class BaseViewModel
    {
        public DateTime? Created { get; set; }

        public DateTime? Updated { get; set; }

        public bool Deleted { get; set; }

        public BaseViewModel()
        {
            Deleted = false;
        }
    }
}
