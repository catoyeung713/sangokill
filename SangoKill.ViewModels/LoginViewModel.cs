﻿using System;

namespace SangoKill.ViewModels
{
    public class LoginViewModel
    {
      public string Username { get; set; }
      public string Password { get; set; }
    }
}
