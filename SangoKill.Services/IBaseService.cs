﻿using SangoKill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SangoKill.Services
{
    public interface IBaseService<M> where M : IBaseModel
    {
        M Add(M domain);
        M GetSingle(Expression<Func<M, bool>> where);
        List<M> GetMultiple(Expression<Func<M, bool>> where);
        List<M> GetAll();
        M Update(M domain);
        void Delete(int id);
        IQueryable<M> GetQueryable();
        IQueryable<M> GetQueryableNoTracking();
    }
}
