﻿using SangoKill.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SangoKill.Services
{
    public interface IUserService : IBaseService<User>
    {
    }
}
