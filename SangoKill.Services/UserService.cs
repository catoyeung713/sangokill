﻿using SangoKill.Interfaces;
using SangoKill.Models;
using SangoKill.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SangoKill.Services
{
    public class UserService : IUserService

    {
        private UserRepository Repository;
        public UserService(UserRepository repository)
        {
            Repository = repository;
        }
        public User Add(User domain)
        {
            return Repository.Add(domain);
        }

        public User GetSingle(Expression<Func<User, bool>> where)
        {
            return Repository.GetSingle(where);
        }

        public List<User> GetMultiple(Expression<Func<User, bool>> where)
        {
            return Repository.GetMultiple(where);
        }

        public List<User> GetAll()
        {
            return Repository.GetAll();
        }

        public User Update(User domain)
        {
            return Repository.Update(domain);
        }

        public void Delete(int id)
        {
            Repository.Delete(id);
        }

        public IQueryable<User> GetQueryable()
        {
            return Repository.GetQueryable();
        }

        public IQueryable<User> GetQueryableNoTracking()
        {
            return Repository.GetQueryableNoTracking();
        }
    }
}
