﻿using SangoKill.Models.EntityBase;
using SangoKill.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SangoKill.Services
{
    public abstract class BaseService<M> where M : BaseModel
    {
        private BaseRepository<M> Repository;
        public BaseService(BaseRepository<M> repository)
        {
            Repository = repository;
        }

        public M Add(M domain)
        {
            return Repository.Add(domain);
        }

        public M GetSingle(Expression<Func<M, bool>> where)
        {
            return Repository.GetSingle(where);
        }

        public List<M> GetMultiple(Expression<Func<M, bool>> where)
        {
            return Repository.GetMultiple(where);
        }

        public List<M> GetAll()
        {
            return Repository.GetAll();
        }

        public M Update(M domain)
        {
            return Repository.Update(domain);
        }

        public void Delete(int id)
        {
            Repository.Delete(id);
        }

        public IQueryable<M> GetQueryable()
        {
            return Repository.GetQueryable();
        }

        public IQueryable<M> GetQueryableNoTracking()
        {
            return Repository.GetQueryableNoTracking();
        }
    }
}
