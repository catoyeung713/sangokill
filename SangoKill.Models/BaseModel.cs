﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SangoKill.Models.EntityBase
{
    public class BaseModel : IBaseModel
    {
        public DateTime? Created { get; set; }

        public DateTime? Updated { get; set; }

        public bool Deleted { get; set; }

        public BaseModel()
        {
            Deleted = false;
        }
    }
}
