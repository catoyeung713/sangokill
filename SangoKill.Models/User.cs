﻿using Microsoft.AspNetCore.Identity;
using SangoKill.Models.EntityBase;
using System;

namespace SangoKill.Models
{
    public class User : IdentityUser, IBaseModel
    {
        public string Name { get; set; }
    }
}
