﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using SangoKill.Models;
using SangoKill.Services;
using SangoKill.ViewModels;

namespace SangoKill.Maps
{
    public class UserMap : EntityMap<User, UserViewModel, UserService>, IUserMap
    {
        public UserMap(UserService service, IMapper mapper) : base(service, mapper)
        {
            
        }
    }
}
