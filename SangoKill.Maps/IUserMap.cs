﻿using SangoKill.Models;
using SangoKill.Services;
using SangoKill.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SangoKill.Maps
{
    public interface IUserMap : IEntityMap<User, UserViewModel, UserService>
    {
    }
}
