﻿using SangoKill.Models;
using SangoKill.Models.EntityBase;
using SangoKill.Services;
using SangoKill.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SangoKill.Maps
{
    public interface IEntityMap<M, VM, S> where M : IBaseModel where VM : IBaseViewModel where S : IBaseService<M>
    {
        VM Create(VM viewModel);
        VM Update(VM viewModel);
    }
}
