﻿using AutoMapper;
using SangoKill.Models;
using SangoKill.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SangoKill.Maps.Profiles
{
    public class ViewModelToDomainProfile : Profile
    {
	    public ViewModelToDomainProfile()
	    {
		    CreateMap<UserViewModel, User>();
	    }
    }
}
