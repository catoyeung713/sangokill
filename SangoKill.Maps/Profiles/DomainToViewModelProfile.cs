﻿using AutoMapper;
using SangoKill.Models;
using SangoKill.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SangoKill.Maps.Profiles
{
    public class DomainToViewModelProfile : Profile
    {
	    public DomainToViewModelProfile()
	    {
		    CreateMap<User, UserViewModel>();
	    }
    }
}
