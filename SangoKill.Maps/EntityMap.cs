﻿using AutoMapper;
using SangoKill.Interfaces;
using SangoKill.Models;
using SangoKill.Models.EntityBase;
using SangoKill.Services;
using SangoKill.ViewModels;
using System;
using System.Collections.Generic;

namespace SangoKill.Maps
{
    public abstract class EntityMap<M, VM, S> : IEntityMap<M, VM, S> where M : IBaseModel where VM : IBaseViewModel where S : IBaseService<M>
    {
        private IMapper Mapper;
        private S Service;
        public EntityMap(S service, IMapper mapper)
        {
            Service = service;
            Mapper = mapper;
        }

        public VM Create(VM viewModel)
        {
            M model = Mapper.Map<M>(viewModel);
            M result = Service.Add(model);
            return Mapper.Map<VM>(result);
        }

        public VM Update(VM viewModel)
        {
            M model = Mapper.Map<M>(viewModel);
            M result = Service.Update(model);
            return Mapper.Map<VM>(result);
        }
    }
}
